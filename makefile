CXX = g++ -Wall -fopenmp
CC = gcc -Wall
CXXFLAGS = -std=c++0x
OPFLAGS = -O3 -march=native -fno-math-errno -fno-trapping-math
LDFLAGS = -lm

all: ROM
	rm -f main.o helper.o pcg64.o

ROM: main.o helper.o pcg64.o
	$(CXX) $? $(LDFLAGS) -o $@

main.o: main.cpp
	$(CXX) $(OPFLAGS) -fopt-info-vec-optimized $? -c

helper.o: helper.cpp
	$(CXX) $(OPFLAGS) $? -c

pcg64.o: pcg64.c
	$(CC) $(OPFLAGS) $? -c


/* This is part of the program that implements the Random Organization Model in 2D
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

# include <iostream>
# include <iomanip>
# include <string>
# include <sstream>
# include <fstream>
# include <cstdlib>
# include <cmath>

# define EXT extern
# include "params.hpp"
# undef EXT

//////////////////////////////////////////////////////////////////////////////

string_code hashit (std::string const& inString)
{
    if (inString == "-phi") return ephi;
    if (inString == "-lx") return elx;
    if (inString == "-N") return en;
    if (inString == "-cycles") return ecycles;
    if (inString == "-gamma") return egamma;
    if (inString == "-maxdisp") return ekick;
    if (inString == "-nens") return enens;
    return enothing;
}

//////////////////////////////////////////////////////////////////////////////

int get_params(int argc, char* argv[])
{
    g_a0 = M_PI * g_dia * g_dia / 4.0;
    
    for(int i = 1; i < argc; i += 2)
    {
        switch(hashit(std::string(argv[i])))
        {
            case elx:
                g_Lx = std::stod(argv[i+1]);
                g_Ly = g_Lx;
                if(g_N > 0)
                {
                    g_Phi = g_N * g_a0 / g_Lx / g_Ly;
                }
                else if(g_Phi > 0)
                {
                    g_N = g_Phi * g_Lx * g_Ly / g_a0;
                }
                break;
            case en:
                g_N = std::stoi(argv[i+1]);
                if(g_Phi > 0)
                {
                    g_Lx = pow(g_N * g_a0 / g_Phi, 1.0 / 2.0);
                    g_Ly = g_Lx;
                }
                else if(g_Lx > 0)
                {
                    g_Phi = g_N * g_a0 / g_Lx / g_Ly;
                }
                break;
            case ephi:
                g_Phi = std::stod(argv[i+1]);
                if(g_N > 0)
                {
                    g_Lx = pow(g_N * g_a0 / g_Phi, 1.0 / 2.0);
                    g_Ly = g_Lx;
                }
                else if(g_Lx > 0)
                {
                    g_N = g_Phi * g_Lx * g_Ly / g_a0;
                }
                break;
            case ecycles:
                g_cycles = std::stoi(argv[i+1]);
                break;
            case egamma:
                g_gamma = std::stod(argv[i+1]);
                break;
            case ekick:
                g_kick = std::stod(argv[i+1]);
                break;
            case enens:
                g_nens = std::stoi(argv[i+1]);
                break;
            case enothing:
                break;
        }
    }
    
    g_phisuf << "_" << g_Phi;
    
    // make directories
    std::string mkdircom1 = "mkdir -p phi" + g_phisuf.str();
    int sysret1 = system(mkdircom1.c_str());
    
    g_CLwidth = g_dia * sqrt(M_PI) * 2.0;
    g_CLlin = lround(g_Lx / g_CLwidth);
    g_CLwidth = g_Lx / (g_CLlin * 1.0);
    g_iCLwidth = 1.0 / g_CLwidth;
    
    if(g_Lx == -1 || g_Phi == -1 || g_N == -1)
    {
        std::cerr << "ERROR: At least two of (phi, N, lx) values need to be entered. Try again. Exiting..." << std::endl;
        return 1;
    }
    
    return 0;
}

//////////////////////////////////////////////////////////////////////////////

void store_conf(double * posx, double * posy, std::string sufx)
{
    std::string confname = "phi" + g_phisuf.str() + "/conf_" + sufx;
    
    std::ofstream conffile (confname.c_str());
    
    conffile.precision(12);
    
    conffile << 0.0 << "\t" << g_Lx << std::endl;
    conffile << 0.0 << "\t" << g_Ly << std::endl << std::endl;
    
    for(int i0 = 0; i0 < g_N; ++i0)
    {
        conffile << posx[i0] << "\t" << posy[i0] << std::endl;
    }
    conffile.close();
    
    return;
}

//////////////////////////////////////////////////////////////////////////////

void store_data(double na, double time)
{
    // average over runs
    na /= g_nens * 1.0;
    time /= g_nens * 1.0;
    
    double fa = na / (g_N * 1.0);
    
    std::ofstream datafile ("data",std::ofstream::out|std::ofstream::app);
    
    datafile.precision(12);
    
    datafile << g_Phi << "\t" << fa << "\t" << time << std::endl;
    
    datafile.close();
    
    return;
}

////////////////////////////////////////////////////////////////////////////

string_code hashit (std::string const&);

int get_params(int, char**);

void store_conf(double *, double *, std::string);

void store_data(double, double);

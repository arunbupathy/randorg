/*
 * PCG Random Number Generation for C.
 *
 * Copyright 2014 Melissa O'Neill <oneill@pcg-random.org>
 * 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional information about the PCG random number generation scheme,
 * including its license and other licensing options, visit
 *
 *     http://www.pcg-random.org
 */

/* This 64 bit minimal C adaptation is by Arunkumar Bupathy <arunbupathy@gmail.com> */

# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>

# include "pcg64.h"

////////////////////////////////////////////////////////////////////////////////

extern inline uint64_t pcg64_random_r(pcg64_random_t*);

////////////////////////////////////////////////////////////////////////////////

extern inline uint64_t pcg64_boundedrand_r(pcg64_random_t*, uint64_t);

////////////////////////////////////////////////////////////////////////////////

extern inline double pcg64_random_r_float53(pcg64_random_t*);

////////////////////////////////////////////////////////////////////////////////

extern inline void pcg64_srandom_r(pcg64_random_t* rng, uint128_t, uint128_t);

////////////////////////////////////////////////////////////////////////////////

int seed_rng_with_entropy(pcg64_random_t * my_rng)
{
    uint128_t seeds[2];
    
    int fd = open("/dev/urandom", O_RDONLY);
    if (fd <= 0)
        return -1;
    
    int sz = read(fd, (void*) seeds, sizeof(seeds));
    
    if (sz < ((int) sizeof(seeds)))
        return -1;
    
    pcg64_srandom_r(my_rng, seeds[0], seeds[1]);
    
    return close(fd);
}

////////////////////////////////////////////////////////////////////////////////

int store_rng_state(pcg64_random_t * my_rng, char * filename)
{
    FILE * fp = fopen(filename, "w");
    
    if(fp == NULL)
    {
        fprintf(stderr, "\nWARNING: Could not write the RNG state to file.\n");
        return -1;
    }
    
    uint128_t st = my_rng->state;
    uint128_t in = my_rng->inc;
    
    for(int i0 = 0; i0 < 4; ++i0)
    {
        fprintf(fp, "%u\t", (unsigned) st);
        st = (st >> 32);
    }
    for(int i0 = 0; i0 < 4; ++i0)
    {
        fprintf(fp, "%u\t", (unsigned) in);
        in = (in >> 32);
    }
    
    fprintf(fp, "\n");
    fclose(fp);
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

int read_rng_state(pcg64_random_t * my_rng, char * filename)
{
    FILE * fp = fopen(filename, "r");
    
    if(fp == NULL)
    {
        fprintf(stderr, "\nERROR: Could not read the RNG state from file!\n");
        return -1;
    }
    
    unsigned st[4];
    unsigned in[4];
    
    int words = 0;
    for(int i0 = 0; i0 < 4; ++i0)
        words += fscanf(fp, "%u\t", &st[i0]);
    for(int i0 = 0; i0 < 4; ++i0)
        words += fscanf(fp, "%u\t", &in[i0]);
    
    my_rng->state = PCG128(PCG64(st[3],st[2]),PCG64(st[1],st[0]));
    my_rng->inc = PCG128(PCG64(in[3],in[2]),PCG64(in[1],in[0]));
    
    fclose(fp);
    
    if(words < 8)
    {
        fprintf(stderr, "\nERROR: Could not read the complete RNG state from file!\n");
        return -1;
    }
        
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

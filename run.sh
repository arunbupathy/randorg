#!/bin/bash 

export OMP_NUM_THREADS=16
time -p ./ROM -phi 0.4 -lx 100 -cycles 10000 -gamma 0.01 -maxdisp 0.3 -nens 16 1>>out 2>>err

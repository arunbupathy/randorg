
/* This is a program to implement the Random Organization Model in 2D, which
 * shows a non-equilibrium phase transition from active to absorbing state
 * as a function of the packing fraction.
 * 
 * N particles are initially randomly distributed in a LxL box (phi = N*pi*r^2/L/L)
 * At each step, the box is sheared by an amplitude 'gamma' and any overlapping
 * particle is marked active. The shear is then reverted and *all* active 
 * particles are given random displacements (up to a max amplitude) until
 * no more active particles are there or when f_a, the fraction of active ples
 * reaches a steady state.
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//////////////////////////////////////////////////////////////////////////////

# include <iostream>
# include <cstdlib>
# include <cmath>
# include <string>
# include <sstream>
# include <fstream>
# include <vector>

//////////////////////////////////////////////////////////////////////////////

# include "pcg64.h"
# define EXT ;
# include "params.hpp"
# undef EXT
# include "helper.hpp"

////////////////////////////////////////////////////////////////////////////

static inline void shear_box(double * posx, double * posy, double g_gamma)
{
    for(int i0 = 0; i0 < g_N; ++i0)
    {
        posx[i0] = posx[i0] + g_gamma*posy[i0];
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

void get_active(int &, int *, double *, double *, double);

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    // these are defaults
    g_Lx = -1;
    g_Ly = -1;
    g_N = -1;
    g_Phi = -1;
    g_gamma = 0.0;
    
    g_dia = 1.0; // changed the dia to 1
    g_cycles = 1000000;
    g_nens = 1;
    g_kick = g_dia / 3.0;
    
    int initstate = get_params(argc, argv);
    if(initstate == 1)  return 1;
    
    double na_av = 0.0;
    double atime_av = 0.0;
    
    # pragma omp parallel for
    for(int iens = 0; iens < g_nens; ++iens)
    {
        // thread private objects, so need to be declared here!
        pcg64_rng my_ran; // the rng state object
        seed_rng_with_entropy(&my_ran);
        
        // position arrays
        double * posx = new double [g_N];
        double * posy = new double [g_N];
        
        // load the lattice with particles
        for(int i0 = 0; i0 < g_N; ++i0)
        {
            posx[i0] = g_Lx * rand_real(&my_ran);
            posy[i0] = g_Ly * rand_real(&my_ran);
        }
        
        // active particles list
        int * act = new int [g_N];
        int na = 0; // number of active sites
        
        shear_box(posx, posy, g_gamma);
        get_active(na, act, posx, posy, g_gamma);
        shear_box(posx, posy, -g_gamma);
        
        int nscount = 0, ns = 1; // next step counter, and next step //
        int time;
        
        for(time = 0; time < g_cycles && na > 0; ++time)
        {
            # ifdef STORE_TSERIES
            if(time % ns == 0)
            {
                ++nscount;
                if(nscount % 16 == 0)
                    ns *= 2;
                /* The code above makes sure that the first 16 configurations are stored at
                 * at an interval of 1 cycle, the next 16 at an interval of 2, the next 16
                 * at an interval of 4, and so on.
                 */
                
                std::ostringstream suffix;
                suffix << iens << "_t" << time;
                store_conf(posx, posy, suffix.str());
            }
            # endif
            
            // pick all active sites and give random displacements (of equal length) to them
            for(int s0 = 0; s0 < na; ++s0)
            {
                int k0 = act[s0];
                
                double theta = 2.0 * M_PI * rand_real(&my_ran);
                double kick = g_kick * rand_real(&my_ran);
                double dx = kick * cos(theta);
                double dy = kick * sin(theta);
                
                // update the x and y coordinates of k0
                posx[k0] += dx;
                posy[k0] += dy;
                
                // pbc
                posx[k0] = posx[k0] - g_Lx * floor(posx[k0] / g_Lx);
                posy[k0] = posy[k0] - g_Ly * floor(posy[k0] / g_Ly);
            }
            
            shear_box(posx, posy, g_gamma);
            get_active(na, act, posx, posy, g_gamma);
            shear_box(posx, posy, -g_gamma);
        }
        
        # pragma omp atomic update
        atime_av += time;
        # pragma omp atomic update
        na_av += na;
        
        std::ostringstream suffix;
        suffix << iens;
        # ifdef STORE_TSERIES
        suffix << "_t" << time;
        # endif
        store_conf(posx, posy, suffix.str());
        
        delete[] act;
        delete[] posx;
        delete[] posy;
    }
    
    store_data(na_av, atime_av);
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////

void get_active(int & na, int * act, double * posx, double * posy, double g_gamma)
{
    na = 0;
    
    std::vector<int> celllist[g_CLlin * g_CLlin];
    // make the cell list
    for(int i0 = 0; i0 < g_N; ++i0)
    {
        int x = posx[i0] * g_iCLwidth;
        int y = posy[i0] * g_iCLwidth;
        int indx = y * g_CLlin + x;
        celllist[indx].push_back(i0);
    }
    
    for(int i0 = 0; i0 < g_CLlin*g_CLlin; i0++)
    {
        // now pick a cell and find its x and y co-ordinates
        int x = i0 % g_CLlin;
        int y = i0 / g_CLlin;
        
        int xp = (x + 1) % g_CLlin;
        int xn = (x - 1 + g_CLlin) % g_CLlin;
        int yp = (y + 1) % g_CLlin;
        int yn = (y - 1 + g_CLlin) % g_CLlin;
        
        int ncell[9];
        // these are the neighbors of the cell i0 (including itself)
        ncell[0] = yp * g_CLlin + xn;
        ncell[1] = yp * g_CLlin + x;
        ncell[2] = yp * g_CLlin + xp;
        ncell[3] = y * g_CLlin + xn;
        ncell[4] = y * g_CLlin + x; // this is equal to i0
        ncell[5] = y * g_CLlin + xp;
        ncell[6] = yn * g_CLlin + xn;
        ncell[7] = yn * g_CLlin + x;
        ncell[8] = yn * g_CLlin + xp;
        
        int max_size = 0, sub = 0;
        // now count the total number of particles in the given cell and its neighbors
        for(int i0 = 0; i0 < 9; ++i0)
        {
            if(i0 == 4)
                sub = max_size; // this is the number of particles in the neighbor cells preceding i0
            max_size += celllist[ncell[i0]].size();
        }
        
        double lposx[max_size], lposy[max_size];
        // now create local arrays to store the position co-ordinates of all the particles in these cells
        for(int i0 = 0, nn = 0; i0 < 9; ++i0)
        {
            int i1 = ncell[i0];
            for(int j0 = 0; j0 < (int) celllist[i1].size(); ++j0, ++nn)
            {
                int jp = celllist[i1][j0];
                lposx[nn] = posx[jp];
                lposy[nn] = posy[jp];
            }
        }
        
        // for every particle in the cell i0, check with every other neighboring p'le to see if they overlap
        for(int i = 0; i < (int) celllist[i0].size(); ++i)
        {
            int noverlap = 0;
            int ip = celllist[i0][i];
            
            for(int jp = 0; jp < max_size; ++jp)
            {
                double diffx = lposx[jp] - posx[ip];
                double diffy = lposy[jp] - posy[ip];
                
                diffx = diffx - g_Ly * g_gamma * round(diffy / g_Ly);
                diffx = diffx - g_Lx * round(diffx / g_Lx);
                diffy = diffy - g_Ly * round(diffy / g_Ly);
                
                double drsq = diffx * diffx + diffy * diffy;
                
                noverlap += (drsq > g_dia * g_dia || i == jp-sub) ? 0 : 1;
                // i == jp-sub makes sure that we're not checking the particle with itself for overlap
            }
            
            // we could have moved this inside the loop above, and exit as soon as we find an overlap
            // but this would prevent the loop from being vectorized
            if(noverlap > 0)
                act[na++] = ip;
        }
    }
    
    return;
}

//////////////////////////////////////////////////////////////////////////////


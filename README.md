# Random Organization Model

A stand-alone program that implements the Random Organization model of Corté *et al.* ([Nature Physics **4**, 420 (2008)](https://www.nature.com/articles/nphys891)). The model is as follows. Initially, `N` particles are randomly distributed in a `L x L` box, with `phi = N pi r^2 / L^2`. At each step (or cycle), the simulation box is sheared by a strain amplitude `gamma`. Any overlapping particles in this state are marked active. The shear is then reverted and *all* active particles are given random displacements (up to a maximum amplitude). This is repeated until there are no more active particles, or until the system reaches a steady state with a fixed fraction of active particles `f_a`.

The code should be self-explanatory, and a run script is provided to explain usage.


enum string_code
{
  ephi,
  elx,
  en,
  ecycles,
  egamma,
  ekick,
  enens,
  enothing
};

// # define STORE_TSERIES

EXT double g_Lx, g_Ly;
EXT double g_Phi, g_a0, g_dia, g_kick;
EXT int g_N;
EXT int g_cycles, g_nens;
EXT std::ostringstream g_phisuf;
EXT double g_CLwidth, g_iCLwidth;
EXT int g_CLlin;
EXT double g_gamma;
